﻿using System;
using CaptchaFun.Controllers;
using CaptchaFun.Models;
using CaptchaFun.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace CaptchaFunTest
{
    public class CaptchaTest
    {
        private ICaptchaService captchaService;
        private CaptchaController controller;
        public CaptchaTest()
        {
            
        }

        [SetUp]
        public void Setup()
        {
            captchaService = new CaptchaService();
            controller = new CaptchaController(captchaService);
        }

        [Test]
        public void TestCaptcha1()
        {
            Input input = new Input
            {
                Format = 0,
                LeftOperand = 1,
                Operator = 1,
                RightOperand = 1
            };
            Assert.AreEqual("1 - one", captchaService.CreateCaptcha(input));
        }

        [Test]
        public void TestCaptchaControllerOk()
        {
            Input input = new Input
            {
                Format = 0,
                LeftOperand = 1,
                Operator = 1,
                RightOperand = 1
            };
            var result = controller.GetCaptcha(input);
            Assert.True(result is JsonResult);
        }

        [Test]
        public void TestCaptchaControllerBadRequest()
        {
            Input input = new Input
            {
                Format = 3,
                LeftOperand = 1,
                Operator = 1,
                RightOperand = 1
            };
            var result = controller.GetCaptcha(input);
            Assert.True(result is BadRequestResult);
        }
    }

}
