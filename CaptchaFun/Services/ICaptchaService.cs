﻿using System;
using CaptchaFun.Models;

namespace CaptchaFun.Services
{
    public interface ICaptchaService
    {
        string CreateCaptcha(Input input);
    }
}
