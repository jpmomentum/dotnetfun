﻿using System;
using System.Collections.Generic;
using CaptchaFun.Models;

namespace CaptchaFun.Services
{
    public class CaptchaService : ICaptchaService
    {

        Dictionary<int,string> numDict = new Dictionary<int, string>()
        {
            { 0, "zero" },
            { 1, "one" },
            { 2, "two" },
            { 3, "three" },
            { 4, "four" },
            { 5, "five" },
            { 6, "six" },
            { 7, "seven" },
            { 8, "eight" },
            { 9, "nine" },
            

        };
        public CaptchaService()
        {

        }

        public string CreateCaptcha(Input input)
        {
            bool convertLeft = false;
            if(input.Format == 0)
            {
                convertLeft = false;
            }else if (input.Format == 1)
            {
                convertLeft = true;
            }
            else
            {
                return null;
            }

            var opChar = getOperatorChar(input.Operator.Value);
            if (String.IsNullOrEmpty(opChar))
            {
                return null;
            }

            try
            {
                if (convertLeft)
                {
                    var leftOperand = numDict[input.LeftOperand.Value];
                    return string.Format("{0} {1} {2}", leftOperand, opChar, input.RightOperand);
                }
                else
                {
                    var rightOperand = numDict[input.RightOperand.Value];
                    return string.Format("{0} {1} {2}", input.LeftOperand, opChar, rightOperand);
                }
            }
            catch
            {
                return null;
            }
        }

        private string getOperatorChar(int input)
        {
            switch (input)
            {
                case 0:
                    return "+";
                case 1:
                    return "-";
                case 2:
                    return "*";
                default:
                    return null;
            }
        }

        private bool isBetween(int num, int lower, int upper)
        {
            return (num >= lower && num <= upper);
        }

    }
}
