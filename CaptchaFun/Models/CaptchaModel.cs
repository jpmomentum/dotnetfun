﻿using System;
namespace CaptchaFun.Models
{
    public class Input
    {
        public int? Format { get; set; }
        public int? LeftOperand { get; set; }
        public int? Operator { get; set; }
        public int? RightOperand { get; set; }
    }
}
