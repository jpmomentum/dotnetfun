﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaptchaFun.Models;
using CaptchaFun.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CaptchaFun.Controllers
{
    [Route("api/[controller]")]
    public class CaptchaController : Controller
    {
        ICaptchaService captchaService;

        public CaptchaController(ICaptchaService captchaService)
        {
            this.captchaService = captchaService;
        }
        [HttpGet("create")]
        public IActionResult GetCaptcha([FromBody] Input input)
        {
            var result = captchaService.CreateCaptcha(input);

            if (!String.IsNullOrEmpty(result))
            {
                return Json(new { result = result });
            }
            return BadRequest();
        }
    }
}
